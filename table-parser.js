function parse(table) {
  if (table === "") {
    return {
      header: [],
      rows: [],
    };
  }

  const headerAndRows = table
    .split("\n")
    .filter((filteredHeadersAndRows) => filteredHeadersAndRows !== "");

  const head = headerAndRows[0];
  const rows = headerAndRows.splice(1);

  const headerName = head
    .split("|")
    .filter((token) => token !== "")[0]
    .trim();

  if (rows.length === 0) {
    return {
      header: [headerName],
      rows: [],
    };
  } else {
    const rowName = rows[0]
      .split("|")
      .filter((token) => token !== "")[0]
      .trim();

    return {
      header: [headerName],
      rows: [rowName],
    };
  }
}

module.exports = parse;
