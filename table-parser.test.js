const parse = require("./table-parser.js");

describe("table-parser", () => {
  it("parses a table with no columns and rows", () => {
    const table = "";
    expect(parse(table)).toEqual({
      header: [],
      rows: [],
    });
  });

  it("parses a table with one column and no rows", () => {
    const table = `| id |`;
    expect(parse(table)).toEqual({
      header: ["id"],
      rows: [],
    });
  });

  it("parses a table with one column and one row", () => {
    const table = `
| id |
| 1 |`;
    expect(parse(table)).toEqual({
      header: ["id"],
      rows: ["1"],
    });
  });

  it("parses a table with one column and two rows", () => {
    const table = `
| id |
| 1 |
| 2 |`;
    expect(parse(table)).toEqual({
      header: ["id"],
      rows: ["1"],
    });
  });
});
